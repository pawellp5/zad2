#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <cassert>
#include "wektor.h"
#include "prostokat.h"
#include "macierz.h"
#include "lacze_do_gnuplota.hh"
#include "menu.h"

Menu::Menu(){
    Pr(A) = Wektor(0,0);
    Pr(B) = Wektor(200,0);
    Pr(C) = Wektor(200,150);
    Pr(D) = Wektor(0,150);
    zapisz();
}

void Menu::operacje(int znak){

    switch (znak){
        case 'q':
            wpisz_wspolrzedne();
            break;
        case 'w':
            wyswietl_Pr();
            break;
        case 'p':
            przesun();
            break;
        case 'm':
            wyswietl_menu();
            break;
        case 's':
            dlugosc_bokow();
            break;
        case 'k':
            cout << "Koniec programu" << endl;
            exit(0);
        case 'o':
            obrot_kat();
            break;
        case 'a':
            wyswietl();
            break;
        default:
            cout << "Podales zla opcje!!!, Podaj prawidlowa" << endl;
            break;
}
}

void Menu::wpisz_wspolrzedne(){
    cout << "Wpisz wspolrzedne prostokata. (zgodnie ze schematem)" << endl;
    cout << "x1 y1" << endl;
    cout << "x2 y2" << endl;
    cout << "x3 y3" << endl;
    cout << "x4 y4" << endl;
    ofstream zapis("prostokat.dat");
    cin >> Pr;
    zapisz();
}

void Menu::wyswietl_Pr(){
    cout << "Wspolrzedne prostokata:" << endl;
    cout << Pr;
}

void Menu::przesun(){
    Wektor Vek;
    cout << "Wpisz wektor o ktory chcesz przesunac prostokat:";
    cin >> Vek;
    cout << endl;
    Pr.przesun(Vek);
    zapisz();
}

void Menu::wyswietl_menu(){
    cout << "m - wyswielt menu" << endl;
    cout << "q - wprowadz wspolrzedne prostokata." << endl;
    cout << "w - wyswietlenie wspolrzednych prostokata" << endl;
    cout << "s - sprawdzenie dlugosci przeciwleglych bokow" << endl;
    cout << "p - przesuniecie prostokata o zadany wektor" << endl;
    cout << "o - obrot prostokata o zadany kat" << endl;
    cout << "a - wyswietl graficznie prostokat" << endl;
    cout << "k - koniec dzialania programu" << endl;
}

bool Menu::dlugosc_bokow(){
    float dl_bok1;
    float dl_bok2;
    float dl_bok3;
    float dl_bok4;
    dl_bok1 = Pr.dl_bokuAB();
    dl_bok2 = Pr.dl_bokuBC();
    dl_bok3 = Pr.dl_bokuCD();
    dl_bok4 = Pr.dl_bokuDA();

    if (dl_bok1==dl_bok3 && dl_bok2==dl_bok4 ){
        cout << "Dlugosci bokow AB i CD oraz BC i DA sa rowne." << endl;
    }else{
        cout << "Dlugosci bokow AB i CD oraz BC i DA nie sa takie same." << endl;
        return false;
    }
    if(dl_bok1 > dl_bok2){
        cout << "Dlugosc dluzszych bokow : " << dl_bok1 << endl;
    }else{
        cout << "Dlugosc krotszych bokow :" << dl_bok2 << endl;
    }
    return true;
}

void Menu::obrot_kat(){
    cout << "Podaj kat ,o ktory chcesz obrocic prostokat, oraz ilosc obrotow:\t";
    float alpha;
    float ilosc_obrotow;
    cin >> alpha;
    cin >> ilosc_obrotow;
    Pr.obracaj(alpha,ilosc_obrotow);
    cout << endl;
    zapisz();
}

void Menu::wyswietl(){
      // 1. Rysowane jako linia ciagl o grubosci 2 piksele

    Lacze.DodajNazwePliku("prostokat.dat",PzG::RR_Ciagly,2);

      // 2. Rysowane jako zbior punktow reprezentowanych przez kwadraty,

    Lacze.DodajNazwePliku("prostokat.dat",PzG::RR_Punktowy,2);

      // Ustawienie trybu rysowania 2D, tzn. rysowany zbiór punktów
      // znajduje się na wspólnej płaszczyźnie. Z tego powodu powoduj
      // jako wspolrzedne punktow podajemy tylko x,y.

    Lacze.ZmienTrybRys(PzG::TR_2D);
    Lacze.Rysuj();
}

void Menu::zapisz(){
    ofstream zapis("prostokat.dat");
    zapis << Pr;
    zapis << Pr(A);
    zapis.close();
}
