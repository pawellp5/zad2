#ifndef TESTS_H
#define TESTS_H
#include <assert.h>
#include "macierz.h"
#include "wektor.h"
#include "menu.h"
#include "prostokat.h"

/**
 * @brief funkcja umolziwiajaca przetestowanie poprawnosci niektorych metod oraz funkcji programu
 */
void testy();

#endif // TESTS_H
