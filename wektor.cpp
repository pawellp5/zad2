#include <iostream>
#include "wektor.h"
using namespace std;
Wektor::Wektor(){
    x=0;
    y=0;
}

Wektor::Wektor(float new_x,float new_y){
    x=new_x;
    y=new_y;
}

Wektor Wektor::dodaj(const Wektor& arg)const{
    Wektor suma;
    suma(X)=x+arg(X);
    suma(Y)=y+arg(Y);
    return suma;
}
Wektor Wektor::odejmij(const Wektor& arg)const{
    Wektor roznica;
    roznica(X)=x-arg(X);
    roznica(Y)=y-arg(Y);
    return roznica;

}

float Wektor::iloczynSkal( const Wektor& v1)const
{
    float wynik = 0;
    wynik=v1(X)*x+v1(Y)*y;
    return wynik;
}

Wektor Wektor::operator +(const Wektor& w )const{
    return dodaj(w);
}

Wektor Wektor::operator -(const Wektor& w)const{
    return odejmij(w);
}

float Wektor::operator *(const Wektor& w1)const{
    return iloczynSkal(w1);
}

bool Wektor::operator ==(const Wektor& w1)const{
    if (w1(X) == x && w1(Y) == y){
        return true;
    }else{
        return false;
    }
}

float Wektor::operator ()(const int &ind)const
{
    if (ind==0){
        return x;
    }else{
        return y;
    }
}

float& Wektor::operator () (const int &ind){
    if (ind==0){
        return x;
    }else{
        return y;
    }
}

ostream & operator << (ostream &StrmWy,Wektor w){
    return StrmWy << w(X) << "\t" << w(Y);
}

istream & operator >> (istream &StrmWe, Wektor& w){
    return StrmWe >> w(X) >> w(Y);

}
