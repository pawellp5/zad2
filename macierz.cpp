#include <iostream>
#include "macierz.h"
#include "wektor.h"

using namespace std;

Macierz::Macierz(){
    tab[0] = Wektor(0,0);
    tab[1] = Wektor(0,0);
}

Macierz::Macierz(float a,float b,float c,float d){
    tab[0] = Wektor(a,c);
    tab[1] = Wektor(b,d);
}

float Macierz::operator()(int wie,int kol)const{
    return tab[kol](wie);
}
float& Macierz::operator()(int wie,int kol){
    return tab[kol](wie);
}

Wektor Macierz::mnozenie (const Wektor& v1){
    Wektor wynik;
    wynik(X) = tab[0](0)*v1(X)+tab[1](0)*v1(Y);
    wynik(Y) = tab[0](1)*v1(X)+tab[1](1)*v1(Y);
    return wynik;
}
Wektor Macierz::operator *(const Wektor& w){
    return mnozenie(w);
}

ostream & operator << (ostream &StrmWy, Macierz w){
    return StrmWy << "(" << w(0,0) << "," << w(0,1) << "," << w(1,0) << "," << w(1,1) << ")";
}

istream & operator >> (istream &StrmWe, Macierz& w){
    return StrmWe >> w(0,0) >> w(0,1) >> w(1,0) >>  w(1,1) ;
}
