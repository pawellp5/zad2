#ifndef MACIERZ
#define MACIERZ
#include <iostream>
#include "wektor.h"

/*!
 * \file  macierz.h
 *
 *  Plik zawiera definicję klasy realizującej interfejs
 *  komunikacyjny do programu gnuplot.
 */


/**
 * @brief Klasa do obslugi macierzy 2D
 */
class Macierz{
public:

    /** \brief domyslny konstruktor klasy - wypelnia macierz zerami
    **/
    Macierz ();

    /** \brief konctruktor inijalicujacy macierz kolejnymi parametrami jak ponizej
      *  |a  b|
      *  |c  d|
      * @param a - element 1 macierzy
      * @param b - element 2 macierzy
      * @param c - element 3 macierzy
      * @param d - element 4 macierzy
    **/
    Macierz(float a,float b,float c, float d);

    /**
     * @brief przeciarzenie operatora sluzace do odczytu elementow macierzy
     * @param wie - wiersz, slozy do okreslenia wiersza w ktorym znajduje sie element macierzy
     * @param kol - kolumne, slozy do okreslenia kolumny w ktorym znajduje sie element macierzy
     * @return WARTOSC elementu macierzy na podstawie przekazanego polozenia elementu w macierzy
      **/
    float operator()(int wie,int kol)const;

    /**
     * @brief umowliwia modyfikacje konkretnego elementu macierzy
     * @param wie - wiersz macierzy`
     * @param kol - kolumna macierzy
     * @return wskazany elemnet macierzy
     */
    float& operator()(int wie,int kol);


    /**
     * @brief przeciazenie operatora mnozenia macierzy przez wektor
     * @param w - wektor przez ktory mnozymy macierz
     * @return wynikowy wektor
     **/
    Wektor operator *(const Wektor& w);

private:
    /**
     * @brief tab - tablica ktora jest wykorzystywana do przechowywania danych macierzy

     */
    Wektor tab[2];
    /**
     * @brief mnozy macierz przez wskazany wektor
     * @param w - wektor przez ktory bedzie pomnozona macierz
     * @return wynik operacji macierz*wektor
     */
    Wektor mnozenie (const Wektor& w);
};

/**
 * @brief operator << przeciazenie operatora strumienia wyjsciowego
 * @param StrmWy - referencja do strumienia wyjsciowego
 * @param w - macierz do wyswietlenia
 * @return zwraca zmodyfikowany strumien wyjsciowy
 */
ostream & operator << (ostream &StrmWy, Macierz w);

/**
 * @brief operator >> przeciazenie operatora strumienia wejsciowego
 * @param StrmWe referencja do strumienia wejsciowego
 * @param w - macierz do modyfikacji
 * @return strumien wejsciowy
 */
istream & operator >> (istream &StrmWe, Macierz& w);

#endif // MACIERZ

