all:Prostokat

Prostokat : main.o macierz.o wektor.o prostokat.o lacze_do_gnuplota.o menu.o tests.o
	g++ -Wall -g main.o macierz.o wektor.o prostokat.o lacze_do_gnuplota.o menu.o tests.o -o Prostokat

main.o : main.cpp
	g++ -Wall -g -c main.cpp -o main.o

wektor.o : wektor.cpp
	g++ -Wall -g -c wektor.cpp -o wektor.o

macierz.o : macierz.cpp
	g++ -Wall -g -c macierz.cpp -o macierz.o

prostokat.o : prostokat.cpp
	g++ -Wall -g -c prostokat.cpp -o prostokat.o

lacze_do_gnuplota.o : lacze_do_gnuplota.cpp
	g++ -Wall -g -c lacze_do_gnuplota.cpp -o lacze_do_gnuplota.o

menu.o : menu.o
	g++ -Wall -g -c menu.cpp -o menu.o

tests.o : tests.o
	g++ -Wall -g -c tests.cpp -o tests.o

dox:
	doxygen config
	chromium html/index.html


clean:
	rm -rf *.o
	rm -rf html
	rm -rf latex
	rm -rf Prostokat
