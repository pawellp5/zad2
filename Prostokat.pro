TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    macierz.cpp \
    prostokat.cpp \
    wektor.cpp \
    lacze_do_gnuplota.cpp \
    menu.cpp \
    tests.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    prostokat.h \
    lacze_do_gnuplota.hh \
    menu.h \
    tests.h \
    macierz.h \
    wektor.h

DISTFILES += \
    makefile \
    prostokat.dat

