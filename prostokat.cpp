#include "prostokat.h"
#include "wektor.h"
#include "macierz.h"
#include <cmath>
#include <iostream>
#include <cstdlib>
#define PI 3.14

using namespace std;

Prostokat::Prostokat(){
    Wektor a(0,0);
    Wektor b(0,0);
    Wektor c(0,0);
    Wektor d(0,0);
}

Prostokat::Prostokat(Wektor new_a, Wektor new_b, Wektor new_c, Wektor new_d ){
    a = new_a;
    b = new_b;
    c = new_c;
    d = new_d;
}

Wektor Prostokat::operator ()(const int &ind )const
{
    switch (ind){
        case 0:
            return a;
        case 1:
            return b;
        case 2:
            return c;
        case 3:
            return d;
        default:
            cerr << "Zly argument dla punktu, wpisz (1,2,3,4)" << endl;
            exit(1);
    }


}

Wektor& Prostokat::operator () (const int &ind){
    switch (ind){
        case 0:
            return a;
        case 1:
            return b;
        case 2:
            return c;
        case 3:
            return d;
        default:
            cerr << "Zly argument dla punktu, wpisz (1,2,3,4)" << endl;
            exit(1);
    }
}

float Prostokat::przelicz_kat(int alpha){
    float wynik;
    wynik = PI * alpha / 180.0;
    return wynik;
}

void Prostokat::obracaj(int alpha, int ilosc_obrotow){
    float kat = przelicz_kat(alpha);
    for(int i=0; i<ilosc_obrotow; ++i){
        Macierz M(cos(kat),-sin(kat),sin(kat),cos(kat));
        a = M*a;
        b = M*b;
        c = M*c;
        d = M*d;
    }

}

void Prostokat::przesun(Wektor V){
    a = a+V;
    b = b+V;
    c = c+V;
    d = d+V;
}

float Prostokat::dl_bokuAB(){
    float wynik;
    Wektor w = b-a;
    wynik = sqrt( w(X)*w(X) + w(Y)*w(Y) );
    return wynik;
}

float Prostokat::dl_bokuDA(){
    float wynik;
    Wektor w = d-a;
    wynik = sqrt( w(X)*w(X) + w(Y)*w(Y) );
    return wynik;
}

float Prostokat::dl_bokuCD(){
    float wynik;
    Wektor w = d-c;
    wynik = sqrt( w(X)*w(X) + w(Y)*w(Y) );
    return wynik;
}

float Prostokat::dl_bokuBC(){
    float wynik;
    Wektor w = c-b;
    wynik = sqrt( w(X)*w(X) + w(Y)*w(Y) );
    return wynik;
}




ostream & operator << (ostream &StrmWy, Prostokat p){
    return StrmWy << p(A) << "\n" << p(B) << "\n" << p(C) << "\n" << p(D) << "\n";
}

istream & operator >> (istream &StrmWe, Prostokat& p){
    return StrmWe >> p(A) >> p(B) >> p(C) >> p(D);

}
