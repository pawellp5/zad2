#ifndef MENU
#define MENU
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cassert>
#include "wektor.h"
#include "prostokat.h"
#include "macierz.h"
#include "lacze_do_gnuplota.hh"

/*!
 * \file  menu.h
 *
 *  Plik zawiera definicję klasy realizującej interfejs
 *  komunikacyjny do programu gnuplot.
 */


/**
 * @brief The Menu klasa do interakcji z uzytkownikiem
 */
class Menu{
public:
    /**
     * @brief Znak - zmienna ktora posluzy nam jako wybur konkretnej metody w switchu
     */
    char Znak;
    /**
     * @brief Pr - prostokat ktory bedziemy modyfikowac i przekrztalcac na prosbe uzytkownika
     */
    Prostokat Pr;
    /**
     * @brief Lacze - Ta zmienna jest potrzebna do wizualizacji
     */
    PzG::LaczeDoGNUPlota Lacze;
    /**
     * @brief Menu - konstruktor wypelniajacy cztery punkty wektora konkretnymi wspolrzednymi (pierwszymi bez prosby uzytkownika)
     */
    Menu();
    /**
     * @brief operacje metoda w ktorej zawarte jest cale menu na podstawie switcha
     * @param znak - zmienna "wybor" do switcha
     */
    void operacje(int znak);
    /**
     * @brief wpisz_wspolrzedne - metoda do wpisywania konkretnych nowych wspolrzednych do prostokata Pr
     */
    void wpisz_wspolrzedne();
    /**
     * @brief wyswietl_Pr - metdoa wyswietlajaca prostokat Pr w gnuplocie
     */
    void wyswietl_Pr();
    /**
     * @brief przesun - metoda ktora przesuwa zadany prostokat o zadany wektor
     */
    void przesun();
    /**
     * @brief wyswietl_menu - wyswietlanie menu.
     */
    void wyswietl_menu();
    /**
     * @brief dlugosc_bokow - metoda poronujaca czy przeciwlegle boki sa takie same oraz ktore boki prostokata sa dluzsze
     * @return true lub false
     */
    bool dlugosc_bokow();
    /**
     * @brief obrot_kat - metoda obracajaca prostokat o kat
     */
    void obrot_kat();
    /**
     * @brief wyswietl - metoda wyswietlajaca wspolrzedne prostokata
     */
    void wyswietl();
    /**
     * @brief zapisz - metoda zapisujaca wspolrzedne prostokata do pliku "prostokat.dat"
     */
    void zapisz();

};



#endif // MENU

