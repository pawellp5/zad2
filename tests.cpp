#include "tests.h"

void testy(){
    Macierz m(1,2,3,4);
    Wektor w(5,6);
    assert(m*w == Wektor(17,39));

    Prostokat p;
    assert(p.przelicz_kat(180) < 3.15);
    assert(p.przelicz_kat(180) > 3.13);

    assert(p.przelicz_kat(90) < 1.58);
    assert(p.przelicz_kat(90) > 1.56);
}
