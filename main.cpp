#include <iostream>
#include <iomanip>
#include <fstream>
#include <cassert>
#include "tests.h"
#include "wektor.h"
#include "prostokat.h"
#include "macierz.h"
#include "lacze_do_gnuplota.hh"
#include "menu.h"
using namespace std;


int main()
{
    testy();
    Menu menu;
    cout << "Menu,wybierz opcje !!!" << endl << endl;
    menu.wyswietl_menu();
    char znak;
    while(!cin.eof()){
        cin >> znak;
        menu.operacje(znak);
    }


    return 0;
}
