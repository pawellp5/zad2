#ifndef WEKTOR_H
#define WEKTOR_H
#include <iostream>

using namespace std;


/*!
 * \file  wektor.h
 *
 *  Plik zawiera definicję klasy realizującej interfejs
 *  komunikacyjny do programu gnuplot.
 */


/**
 * @brief enum ktore pomoze nam do dostawania sie do danej wspolrzednej wektora
 */
enum Wspolrzedna{
    X,Y
};
/**
 * @brief The Wektor klasa wykorzystywana do reprezentacjci wektora
 */
class Wektor{
public:
    /**
     * @brief domyslny konstruktor klasy inicjalizuje zerowy wektor
     */
    Wektor();
    /**
     * @brief Wektor konstruktor pozwalajacy inicjalizowac wektora poprzez podanie dlugosci i szerokosci
     * @param new_x - nowa wartosc ktora bedzie wpisywana do spolrzednej x wektora
     * @param new_y - nowa wartosc ktora bedzie wpisywana do spolrzednej y wektora
     */
    Wektor(float new_x,float new_y);
    /**
     * @brief operator ()  przeciarzenie operatora sluzace do odczytu  wektora
     * @param ind - indeksc ktory umozliwi nam dostep do dlugosci lub szerokosci
     * @return watosc danej wspolrzednej wektora
     */
    float operator () (const int &ind)const;
    /**
     * @brief operator operator umozliwiajacy nam wpisanie wartosci do konkretnego pola wektora
     * @param ind - indeksc ktory umozliwi nam dostep do dlugosci lub szerokosci wektora
     * @return watosc danej wspolrzednej wektora
     */
    float& operator () (const int &ind);
    /**
     * @brief operator + - przeciazenie operatora dodawania
     * @param w wektor ktory bedziemy dodawac
     * @return wektor wynikowy
     */
    Wektor operator +(const Wektor& w)const;
    /**
     * @brief operator -przeciazenie operatora odejmowania
     * @param w - wektor ktory bedziemy odejmowac
     * @return  wektor wynikowy
     */
    Wektor operator -(const Wektor& w)const;
    /**
     * @brief operator * przeciazenie operatora mnozenia
     * @param w1 - wektor przez ktory bedziemy mnozyc
     * @return wektor wynikowy
     */
    float operator *(const Wektor& w1)const;
    /**
     * @brief operator == przeciazenie operatora porownania
     * @param w1 wektor porownywany
     * @return wartosc bool true lub false
     */
    bool operator ==(const Wektor& w1)const;
private:
    /**
     * @brief x wspolrzedna x
     * @brief y wspolrzedna y
     */
    float x,y;
    /**
     * @brief iloczynSkal metoda mnozaca wektorory przez siebie
     * @param V1 wektor przez ktory bedziemy mnozyc wektor klasy
     * @return wartosc
     */
    float iloczynSkal(const Wektor& V1) const;
    /**
     * @brief dodaj - metoda dodajaca wektor do wektora
     * @param arg - wartosc dodawana
     * @return wektor wynikowy
     */
    Wektor dodaj(const Wektor& arg)const;
    /**
     * @brief odejmij - metoda odejmujaca wektor do wektora
     * @param arg - wektor ktory bedzie dodawany
     * @return wektor wynikowy z operacji
     */
    Wektor odejmij(const Wektor& arg)const;
   };
    /**
    * @brief operator << przeciazenie operator wyswietlania
    * @param StrmWy - referancja do strumienia wyjsciowego
    * @param w wektor ktory bedzie wyswietlany i modyfikowany
    * @return strumien wyjsciowy
    */
    ostream & operator << (ostream &StrmWy, Wektor w);
    /**
    * @brief operator >> przeciazenie operator wpisywania
    * @param StrmWy - referancja do strumienia wejsciowego
    * @param w wektor ktory bedzie  modyfikowany
    * @return strumien wejsciowy
    */
    istream & operator >> (istream &StrmWe, Wektor& w);







#endif // WEKTOR_H
