#ifndef PROSTOKAT
#define PROSTOKAT
#include <iostream>
#include "wektor.h"
#include "macierz.h"
#include <cmath>


/*!
 * \file  prostokat.h
 *
 *  Plik zawiera definicję klasy realizującej interfejs
 *  komunikacyjny do programu gnuplot.
 */




/**
 * @brief The Punkt enum sluzace nam do oznaczenia wektorow w prostokacie
 */
enum Punkt{
    A,B,C,D
};

/**
 * @brief The Prostokat - klasa do obslugi prostokatu zbudowanego na wektorach
 */
class Prostokat{
public:
    /**
     * @brief Prostokat - domyslny konstruktor klasy - wypelnia prostokat zerami

    * @brief  D----------------------C
    * @brief  |                      |
    * @brief  |                      |
    * @brief  A----------------------B
     */
    Prostokat();
    /**
     * @brief Prostokat - konstruktor inicjalizujacy dany punkt prostokata konkretnym wektorem
     * @param new_a - nowy wektor do opisu punktu prostokata
     * @param new_b - nowy wektor do opisu punktu prostokata
     * @param new_c - nowy wektor do opisu punktu prostokata
     * @param new_d - nowy wektor do opisu punktu prostokata
     */
    Prostokat( Wektor new_a, Wektor new_b, Wektor new_c, Wektor new_d );
    /**
     * @brief operator umozliwiajacy odczytanie konkretnego pola prostokata
     * @param ind - indeks umozliwiajacy dostanie nam sie do konkretnego wektora prostokata
     * @return  wektor do ktorego chcielismy sie dostac
     */
    Wektor operator ()(const int &ind)const;
    /**
     * @brief operator operator umozliwiajacy wpisywanie wartosci do konkretnego wektora prostokata
     * @param ind - indeks umozliwiajacy nam wpisanie do konkretnego wektora zadanej wartosci
     * @return wektor wynikowy
     */
    Wektor& operator () (const int &ind);
    /**
     * @brief metoda umozliwiajaca obrocenie prostokata o zadany kat
     * @param alpha - kat o ktory bedziemy obracac prostokat
     * @param ilosc_obrotow - ilosc obrotow ktore dokonamy na danym prostokacie
     */
    void obracaj(int alpha,int ilosc_obrotow);
    /**
     * @brief metoda umozliwiajaca przesuwanie prostokata o zadany wektor
     * @param V - wektor o ktory bedziemy przesuwac prostokat
     */
    void przesun(Wektor V);
    /**
     * @brief dl_bokuAB - dlugosc boku od punktu A do B
     * @return wartosci dlugosci boku
     */
    float dl_bokuAB();
    /**
     * @brief dl_bokuDA - dlugosc boku od punktu D do A
     * @return wartosci dlugosci boku
     */
    float dl_bokuDA();
    /**
     * @brief dl_bokuCD - dlugosc boku od punktu C do D
     * @return wartosci dlugosci boku
     */
    float dl_bokuCD();
    /**
     * @brief dl_bokuBC - dlugosc boku od punktu C do B
     * @return wartosci dlugosci boku
     */
    float dl_bokuBC();
    /**
     * @brief metoda przeliczajaca wartosc katu na radiany
     * @param alpha - wartosc katu ktora bedzie przeliczana
     * @return
     */
    float przelicz_kat(int alpha);
private:
    /**
     * @brief wektor a ktory w prostokacie posluzy nam jako wspolrzedne punktu
     */
    Wektor a;
    /**
     * @brief wektor b ktory w prostokacie posluzy nam jako wspolrzedne punktu
     */
    Wektor b;
    /**
     * @brief wektor c ktory w prostokacie posluzy nam jako wspolrzedne punktu
     */
    Wektor c;
    /**
     * @brief wektor d ktory w prostokacie posluzy nam jako wspolrzedne punktu
     */
    Wektor d;


};

/**
 * @brief operator << - przeciazenie operatora do wyswietlenia prostokata
 * @param StrmWy - referencja do strumienia wyjsciowego
 * @param p - prostokat do wyswietlania i modyfikacji
 * @return strumien wyjsciowy
 */
ostream & operator << (ostream &StrmWy, Prostokat p);
/**
 * @brief operator >> przeciazenie operatora do wpisania wartosci do prostokata
 * @param StrmWe referencja do strumienia wejsciowego
 * @param w - prostokat do modyfikacji
 * @return strumien wejsciowy
 */
istream & operator >> (istream &StrmWe, Prostokat& w);


#endif // PROSTOKAT

